=== SurveyFunnel Pro - Survey Plugin for WordPress ===
Contributors: WPEka Club
Donate link: https://www.surveyfunnel.com/
Tags: surveys, survey tool, questionnaire, custom form, form builder, form maker, forms, lead generation, evaluation.
Requires at least: 5.0
Tested up to: 5.8
Requires PHP: 7.0
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create and embed high-conversion survey forms for generating leads. Design survey forms with drag & drop builder and turn your WordPress site into a lead generation funnel.

== Description ==

SurveyFunnel is an easy-to-use survey plugin for WordPress that lets you create interesting and engaging survey forms with drag & drop options without any coding. You can create surveys to get insights about your audience, identify & segment your leads, and increase lead conversions with personalized marketing.

This WordPress survey plugin offers many features like drag & drop survey builder, custom display pages, long & short answers, radio buttons & checkboxes, opt-in forms, custom results pages, and many more. [Read More](https://www.surveyfunnel.com/surveyfunnel-features/)

All these features ensure that you gain higher engagement rates from your website users and generate more leads.

The plugin is designed to make it super easy for you to create engaging surveys for your audience within minutes.

== Features ==

SurveyFunnel offers you a wide range of features you need to engage your audience, learn about them and convert them into quality leads:

- Create survey forms within minutes with <strong>drag & drop</strong> options without any code. Easily add custom fields and arrange them in the order you want.
- <strong>Custom cover pages</strong> to grab users' attention and get more engagements.
- The survey forms are <strong>responsive</strong> and work perfectly on all devices from mobiles to desktops.
- All types of questions and answers; Single with <strong>radio buttons</strong> or MCQs with <strong>checkboxes</strong>, and more.
- Create <strong>Opt-in forms</strong> for capturing leads and contact them further for updates, promotions, and marketing.
- <strong>Custom survey styles</strong> to design your survey forms with different fonts, colors, background images, and buttons.
- Custom result pages for surveys; show a <strong>thank-you page</strong> on survey completion.
- Get <strong>detailed statistics for each survey form</strong> and questions. The number of form completions, most answered questions, number of opt-ins, etc.
- Get detailed stats and <strong>reports for multiple or individual users</strong>, evaluate their responses, and segment accordingly.
- Export the <strong>survey results in CSV</strong> format for offline analysis.

== Why SurveyFunnel? ==

SurveyFunnel is a part of the premium quality plugins family by WPeka. Here are some compelling reasons to trust the brand and choose this plugin:

- Over 10 years in the WordPress plugins and themes space
- Trusted and actively used by more than 20,000+ customers
- Featured on [AppSumo](https://appsumo.com/products/marketplace-wp-adcenter/)
- Featured on [WPLift](https://wplift.com/wordpress-plugins/plugin-directory/wp-adcenter)
- Featured on [WPMayor](https://wpmayor.com/ad-management-in-wordpress-comparing-four-top-plugins/)
- Plugins with over 30,000+ active installs

WPeka offers premium WordPress Plugins. The team is backed by experts in the WordPress domain with a proven track record of creating top-notch products.

== Attract & Engage ==

Grab your users’ attention and engage them with interesting, smooth, and responsive forms.

== Know Your Users ==

Know the intent and needs of your audience with thoughtful surveys with multiple options to answer.

== Generate Leads ==

Identify and segment the qualified leads based upon the results and plan a more personalized marketing strategy.

== Our other Awesome Projects ==

If you like this plugin, then consider checking out our other projects:

- [WP Legal Pages](https://wplegalpages.com/?utm_source=wporg&utm_medium=referral&utm_campaign=surveyfunnel): Generate 25+ legal policy pages for your WordPress website in just a few minutes, including, Privacy Policy, Terms & Conditions, Cookie Policy, and many more.

- [WP Cookie Consent](https://wplegalpages.com/cookie-consent-banner-on-your-website/?utm_source=wporg&utm_medium=referral&utm_campaign=surveyfunnel): Display a customized cookie consent notice (for GDPR), and "Do Not Sell" opt-out notice (for CCPA). Get granular consent and record a consent log.

- [WP Woo Auction Software](https://wpauctionsoftware.com/?utm_source=wporg&utm_medium=referral&utm_campaign=surveyfunnel): Host eBay like auctions or Simple / Reverse / Penny Auctions on your WooCommerce website.

== Installation ==

Installation is done in few easy steps mentioned below:

1. Download the Surveyfunnel Lite Plugin
2. Log in as the WordPress Admin of your website
3. Navigate to Plugins > Add New to upload plugin
4. Install & Activate the plugin

== Frequently Asked Questions ==

= Who should use SurveyFunnel? =
SurveyFunnel can be used by all marketers and site owners who want to increase engagement on their website, collect more leads, and send more personalized and targeted marketing messages to their audience.

= What types of questions and answers can I make using SurveyFunnel? =
You can create all types of questions and answers with SurveyFunnel including short & long text answers, single choice questions with radio buttons, multiple choice questions with checkboxes, opt-ins, file upload, image questions, etc.

= What statistics do I get?=
In the statistics section within the plugin, you can get all the data related to the number of form completions, opt-ins, users with the most answers, the most answered questions, and much more.

= Can I download the data? =
Yes, you can export and download all the survey data in CSV format to save on your computer.

= Can I display the survey on specific pages or posts? =
Yes, you can place surveys on specific posts or pages that you think are suitable for audience engagement.

== Screenshots ==
1. Dashboard
2. Build Tab
3. Build Tab - Covers page
4. Build Tab - Adding questions.
5. Build Tab - Adding form to the survey.
6. Build Tab - Results page.
7. Design Tab
8. Configure Tab
9. Deploy Tab
10. Reports Tab

== Changelog ==
= 1.0.0 =
* Initial release

== Upgrade Notice ==
= 1.0.0 =
* Initial release
