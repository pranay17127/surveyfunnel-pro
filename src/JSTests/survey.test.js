/**
 * @jest-environment jsdom
 */
import testFunctions from "../Survey";

test ( 'render checkbox in front end on cover page of survey', () => {
	let data = '';
	let configure = {
		proSettings :{
			privacyPolicy: {
				text: 'something',
				link: {
					value: 'http://www.google.com'
				}
			}
		}
	};
	let state = {
		privacyPolicy: true,
	}
	let jsx = testFunctions.renderPrivacyPolicyOption(data, configure, state);

	expect(jsx.props.children.type === 'div').toBeTruthy();
	state.privacyPolicy = false;
	jsx = testFunctions.renderPrivacyPolicyOption(data, configure, state);
	expect(jsx.props.children === '').toBeTruthy();

	configure.proSettings.privacyPolicy.text = '';
	jsx = testFunctions.renderPrivacyPolicyOption(data, configure, state);
	expect(jsx.props.children === '').toBeTruthy();
} )

test( 'validation for cover page in front end', () => {

	let iframeRef = {
		current: {
			node: {
				contentDocument: {
					getElementById: function() {
						return {
							checked: true,
						}
					}
				}
			}
		}
	}

	let error = [];
	let item = {
		privacyPolicy: true,
	}

	let configure = {
		proSettings: {
			privacyPolicy: {
				text: 'something',
				link: {
					value: 'http://www.google.com'
				}
			}
		}
	}

	let flag = testFunctions.checkCoverPageButtonValidations(false, item, iframeRef, error, configure);
	expect(flag).toBeFalsy();
	expect(error.length === 0).toBeTruthy();

	iframeRef = {
		current: {
			node: {
				contentDocument: {
					getElementById: function() {
						return {
							checked: false,
						}
					}
				}
			}
		}
	}

	flag = testFunctions.checkCoverPageButtonValidations(false, item, iframeRef, error, configure);
	expect(flag).toBeTruthy();
	expect(error.length === 1).toBeTruthy();

	configure = {
		proSettings: {
			privacyPolicy: {
				text: ''
			}
		}
	};
	error = [];

	flag = testFunctions.checkCoverPageButtonValidations(false, item, iframeRef, error, configure);
	expect(flag).toBeFalsy();
	expect(error.length === 0).toBeTruthy();
} )