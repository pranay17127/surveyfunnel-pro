import testFunctions from '../../../Components/Design/DesignPreview.js';

test( 'Start Screen Privacy Policy checkbox render in modalbox', () => {
	let data = '';
	let proSettings = {
		privacyPolicy: {
			text: 'something',
			link: {
				value: 'http://www.google.com'
			}
		}
	};
	let state = {
		privacyPolicy: true,
	}
	let jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);

	expect(jsx.props.children.type === 'div').toBeTruthy();
	state.privacyPolicy = false;
	jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();

	proSettings.privacyPolicy.text = '';
	jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();
} )