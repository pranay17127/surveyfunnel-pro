const { addFilter } = wp.hooks;
import React from 'react';

addFilter( 'renderPrivacyPolicyOption', 'Survey', renderPrivacyPolicyOption );

function renderPrivacyPolicyOption (data, configure, item, src) {

	let disabled = false;
	if ( configure.proSettings?.privacyPolicy.text === '' || configure.proSettings?.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = item?.privacyPolicy && ! disabled;
	let privacyPolicyText = 'Privacy Policy';
	let privacyPolicyLink = '';
	if (configure?.proSettings) {
		privacyPolicyText = configure.proSettings.privacyPolicy.text;
		privacyPolicyLink = configure.proSettings.privacyPolicy.link.value;
	}
	return <>
		{privacyPolicy ? <div className="privacyPolicy">
			<p><input type="checkbox" id="privacyPolicyEnabled" /><label htmlFor="privacyPolicyEnabled">
					<span>
						<img src={src} alt="Privacy Policy Checkbox" />
					</span>
				</label> {privacyPolicyText} <a href={privacyPolicyLink} target="_blank">Learn More</a></p>
		</div> : ''}
	</>
}

addFilter( 'checkCoverPageButtonValidations', 'Survey', checkCoverPageButtonValidations );

function checkCoverPageButtonValidations(flag, item, iframeRef, error, configure) {
	let disabled = false;
	if ( configure.proSettings?.privacyPolicy.text === '' || configure.proSettings?.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = item?.privacyPolicy && ! disabled;

	if ( privacyPolicy ) {
		if ( iframeRef.current?.node?.contentDocument.getElementById('privacyPolicyEnabled').checked ) {
			return false;
		}
		error.push(<p key={error.length}>Kindly accept the Privacy Policy to proceed.</p>);
		return true;
	}

	return false;
}

let exportFunctionsForTesting = { renderPrivacyPolicyOption, checkCoverPageButtonValidations };
export default exportFunctionsForTesting;