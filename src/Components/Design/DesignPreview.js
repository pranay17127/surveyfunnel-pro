const { addFilter } = wp.hooks;
import React from 'react';

addFilter('renderPrivacyPolicy', 'DesignPreview', renderPrivacyPolicy)

function renderPrivacyPolicy( data, item, proSettings, src ) {
	let disabled = false;
	if ( proSettings.privacyPolicy.text === '' || proSettings.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = item?.privacyPolicy && ! disabled;
	return <>
		{privacyPolicy ? <div className="privacyPolicy">
			<p><input type="checkbox" id="privacyPolicyEnabled" /><label htmlFor="privacyPolicyEnabled">
					<span>
						<img src={src} alt="Privacy Policy Checkbox" />
					</span>
					</label> {proSettings.privacyPolicy.text} <a href={proSettings.privacyPolicy.link.value} target="_blank">Learn More</a></p>
		</div> : ''}
	</>
}

let exportFunctionsForTesting = { renderPrivacyPolicy };
export default exportFunctionsForTesting;