module.exports = {
	testPathIgnorePatterns: ['/node_modules/'],
	moduleNameMapper: {
		
	},
	setupFilesAfterEnv: ['<rootDir>/jest-setup.js'],
	globals: {
		window: {
			"__DEV__": true,
			"__RCTProfileIsProfiling": false
		}
	},
	"automock": false,
  	"resetMocks": false,
}